from rabbitmqAPI import messageController
from neo4jAPI import neo4j
import sys, time, os

class agent:

    def __init__(self):
        #rabbitmq message manager
        self.mailman = messageController()
        #neo4j cypher query manager
        self.map = neo4j()

        self.agentID = os.getenv("AGENTID")
        self.startNode = os.getenv("STARTNODE")
        self.travelNode = os.getenv("TRAVELNODE")
        self.intelligent = os.getenv("INTELLIGENT")
        self.currentNode = None

        #for results logging
        self.time = time.time()
        self.routeLength = None
        self.resultsStorage = []

        #create incoming message queue for agent
        self.mailman.createQueue(self.agentID)
        print ("\nIncoming message queue for agent created")

        #set initial map position
        print ("\nSetting initial map position")
        self.setMapPosition(self.startNode)

    def __del__(self):
        #close connection to rabbitmq
        del self.mailman

    def traverseIndefinitely(self):
        while(True):
            print("Traverse from node", self.currentNode, "to node", self.travelNode)
            self.traverse(self.travelNode)
            print("Traverse from node", self.currentNode, "to node", self.startNode)
            self.traverse(self.startNode)

    def traverse(self, destinationNode):
        #start time to measure how long function takes
        startTime = time.time()
        if (self.intelligent == "True"):
            #get list of all routes from current location to destination
            routes = self.findAllShortestPaths(destinationNode)
        else:        
            #get shortest route from current location to destination
            #must be a list for later code to work
            routes = [self.findShortestPath(destinationNode)]

        #for each step on the way to destination
        for i in range (0, len(routes[0])):
            #get list of all unique options
            nextNodeList = []
            for j in routes:
                if j[i] not in nextNodeList:
                    nextNodeList.append(j[i])

            if (self.intelligent == "True"):
                #if only one option to go to don't waste any time or system resources
                if (len(nextNodeList) is 1):
                    #broadcast where agent is going next
                    self.publishNextNode(nextNodeList[0])
                    #unbind from current queue
                    self.mailman.unbindQueueFromExchange(str(self.agentID), str(self.currentNode))
                    #as not reading messages clear queue
                    self.mailman.clearQueue(str(self.agentID))
                    #move to only node option
                    self.setMapPosition(nextNodeList[0])
                    #skip straight to next iteration of loop
                    continue

            #For all next unique nodes, see which has least agents at it and store that value
            #Indexes same as in nextNodeList
            nextNodeWeight = self.numAgentsAtNodes(nextNodeList)

            #For all agents at current node see where they are going and adjust weights as appropriate
            #first unbind from queue to limit messages need to check
            self.mailman.unbindQueueFromExchange(str(self.agentID), str(self.currentNode))
            #read all messages and adjust nextNodeWeighting list as appropriate
            otherAgentDestinations = self.neighbourAgentNextNode()
            #amend nextNodeWight using these values
            for j in otherAgentDestinations:
                try:
                    #see if agent is considering the node as next step
                    index = nextNodeList.index(j)
                    nextNodeWeight[index] += 1
                except:
                    #if not in list try next one
                    continue
            #look at weights and calculate which node has lowest/best weight (least nodes at/going to it)
            bestNode = 0
            for j in range (1, len(nextNodeWeight)):
                if nextNodeWeight[bestNode] > nextNodeWeight[j]:
                    bestNode = j

            #broadcast where agent is going next
            self.publishNextNode(nextNodeList[bestNode])

            #move to next node
            self.setMapPosition(nextNodeList[bestNode])

            if (self.intelligent == "True"):
                #remove routes that are no longer viable due to prior decisions 
                #has to happen at end after bestNode is chosen so doesn't interfere
                #decrements through list otherwise deletes items and tries to read indexes that then no longer exist
                for j in range (len(routes)-1, -1, -1):
                    if routes[j][i] != self.currentNode:
                        routes.pop(j)

        #as all intermediary nodes have been used just need broadcast next step and travel to final destination node
        self.publishNextNode(str(destinationNode))
        #unbind from current queue
        self.mailman.unbindQueueFromExchange(str(self.agentID), str(self.currentNode))
        #as not reading messages purge queue
        self.mailman.clearQueue(str(self.agentID))
        #move to only node option
        self.setMapPosition(destinationNode)

        #store route length, +1 to account for destination node
        self.routeLength = len(routes[0]) + 1

        #stop time and calculate function runtime
        stopTime = time.time()
        #convert to milliseconds
        runTime = (stopTime - startTime)/self.routeLength* 1000
        
        #store results
        self.resultsLogging(runTime)

    def resultsLogging(self, runTime):
        self.resultsStorage.append(runTime)
        #every 30 seconds publish results
        if (time.time() - self.time > 30):
            #calculate average runTime
            sumOfRunTime = sum(self.resultsStorage)
            averageRuntime = sumOfRunTime/len(self.resultsStorage)
            #clear old results
            self.resultsStorage.clear()
            #send result
            self.mailman.sendMessage('', 'results', '{"Agent": "' + str(self.agentID) + '", "RouteLength": "' + str(self.routeLength) + '", "RefreshRate": "' + str(int(averageRuntime)) + '"}')
            print ("\nSent half minute results update")
            #reset timer
            self.time = time.time()

    def neighbourAgentNextNode(self):
        #queue to store where other agents at same node going next
        nextLocations = []
        #keep getting messages from incoming queue until empty
        while(True):
            output = self.mailman.getMessage(str(self.agentID), True)
            #whilst queue is not empty decoding will succeed
            try:
                nextLocations.append(output[2].decode('utf-8'))
            #when queue is empty break out of loop
            except:
                break
        return nextLocations

    def numAgentsAtNodes(self, list):
        queries = []
        #for all nodes that we want a count of how many agents are there
        for i in list:
            #build the cypher query and store in queries list
            queries.append("match (n) where n.Name='"+str(i)+"' return size(keys(n))")
        output = self.map.cypherQueries(queries)
        result = []
        #parse json response and return list of how many agents are at each node 
        #result for nodes in same order nodes were passed into function
        for i in output[1]['results']:
            result.append(i['data'][0]['row'][0])
        return result
    
    def publishNextNode(self, nextNode):
        #publish where agent is going next to node exchange
        self.mailman.sendMessage("", str(self.currentNode), str(nextNode))
        #sleep for 100ms before continuing so its more likely
        #that other agents have time to see where you're going
        time.sleep(0.025)

    def findAllShortestPaths(self, destinationNode):
        output = self.map.cypherQueries([
            "MATCH(a{Name:'"+str(self.currentNode)+"'}),(b{Name:'"+str(destinationNode)+"'}),path=allShortestPaths((a)-[*]->(b))return path"
            ])
        if (output[0]!='Success'):
            return None
        #ugly json parse as neo4j returns very ugly responses
        list = output[1]['results'][0]['data']
        shortestPaths = []
        #extract the json responses
        for i in list:
            currentPath = []
            #need to increment by 2 here as every other entry is relationship
            #starts from 2 to ignore currentNode
            #len(i['row'][0])-2 to ignore destination node as we already know that step
            for j in range (2, len(i['row'][0])-2, 2):
                currentPath.append(i['row'][0][j]['Name'])
            shortestPaths.append(currentPath)
        return shortestPaths

    def findShortestPath(self, destinationNode):
        output = self.map.cypherQueries([
            "MATCH(a{Name:'"+str(self.currentNode)+"'}),(b{Name:'"+str(destinationNode)+"'}),path=shortestPath((a)-[*]->(b))return path"
            ])
        if (output[0]!='Success'):
            return None
        #ugly json parse as neo4j returns very ugly responses
        list = output[1]['results'][0]['data'][0]['row'][0]
        #need to increment by 2 here as every other entry is relationship
        #starts from 2 to ignore currentNode
        #len(list)-2 to ignore destination node as we already know that step
        shortestPath = []
        for i in range (2, len(list)-2, 2):
            shortestPath.append(list[i]['Name'])
        return shortestPath

    def setMapPosition(self, newNode):
        #Delete old position and set new position, queries execute one after another on neo4j, should prevent race conditions
        self.map.cypherQueries([
            "match (n) where n.Name='" + str(self.currentNode) + "' remove n.a" + str(self.agentID),
            "match (n) where n.Name='" + str(newNode) + "' set n.a" + str(self.agentID) + "=1"
            ])
        #bind to node exchange
        self.mailman.bindQueueToExchange(str(self.agentID), str(newNode))
        self.currentNode = newNode
        print ("Agent at node", self.currentNode)

def main():
    #create instance of agent
    instance = agent()
    #traverse map indefinitely
    instance.traverseIndefinitely()


if __name__ == "__main__":
    main()