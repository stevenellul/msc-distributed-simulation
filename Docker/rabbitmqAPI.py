import pika, os

#class only has error handling for things that will affect functionality, any other crashes will require a restart so not handled
class messageController:

    #Creates connection on construction
    def __init__(self):
        self.url = os.getenv("RABBITMQURL")
        self.port = os.getenv("RABBITMQPORT")
        self.vHost = os.getenv("RABBITMQVHOST")
        self.credentials = pika.PlainCredentials(os.getenv("RABBITMQUSERNAME"), os.getenv("RABBITMQPASSWORD"))
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(self.url, self.port, self.vHost, self.credentials))
        self.channel = self.connection.channel()

    #Close connection on destruction
    def __del__(self):
        self.connection.close()

    def createQueue(self, desiredQueue):
        self.channel.queue_declare(queue = desiredQueue)

    def createExchange(self, desiredExchange, exchangeType):
        self.channel.exchange_declare(exchange = desiredExchange, exchange_type = exchangeType)

    def clearQueue(self, desiredQueue):
        self.channel.queue_purge(queue = desiredQueue)

    def deleteExchange(self, desiredExchange):
        self.channel.exchange_delete(exchange = desiredExchange)

    def bindQueueToExchange(self, desiredQueue, desiredExchange):
        self.channel.queue_bind(queue = desiredQueue, exchange = desiredExchange)

    def unbindQueueFromExchange(self, desiredQueue, desiredExchange):
        self.channel.queue_unbind(queue = desiredQueue, exchange = desiredExchange)

    def deleteQueue(self, desiredQueue):
        self.channel.queue_delete(queue = desiredQueue)

    def sendMessage(self, desiredQueue, desiredExchange, message):
        self.channel.basic_publish(exchange = desiredExchange, routing_key = desiredQueue, body = message)

    def getMessage(self, desiredQueue, acknowledge):
        try:
            #no_ack = true flag means auto-acknowledge, no acknowledgement needed, remove it to manually acknowledge messages
            method_frame, header_frame, body = self.channel.basic_get(desiredQueue, no_ack = acknowledge)
            return (method_frame, header_frame, body)
        except:
            #when a getMessage fails the channel is closed, so here we reopen it
            self.channel = self.connection.channel()
            #essentially notifies that there is no queue or message
            return (None, None, None)
    
    #only works without no_ack
    def acknowledgeMessage(self, method_frame):
        try:
            self.channel.basic_ack(method_frame.delivery_tag)
        except:
            #if there is no message to acknowledge we don't care so do nothing
            None

    #only works without no_ack
    def rejectMessage(self, method_frame, requeue):
        try:
            #requeue = true flag means message will be requeued
            self.channel.basic_reject(method_frame.delivery_tag, requeue=requeue)
        except:
            #If there is no message to reject we don't care so do nothing
            None

def exchangeExperiment():
    print ("hello :)")
    mailman = messageController()
    mailman.createExchange("hello", 'fanout')
    mailman.createQueue("1")
    mailman.createQueue("2")
    mailman.unbindQueueFromExchange("2","hello")
    mailman.sendMessage('', 'hello', "message")
    mailman.deleteExchange('hello')

def test():
    mailman = messageController()
    mailman.createQueue("test")
    mailman.sendMessage('', "test", "hey guys")

    #incorrect get message from non existent queue closes channel
    #sendmessage is fine, create queue is fine,  delete queue is fine, acknowledge is fine, reject is fine
    output = mailman.getMessage("tst", False)

    output = mailman.getMessage("test", False)
    mailman.rejectMessage(output[0], True)
    #proper way to decode messages
    print (output[2].decode('utf-8'))
    output = mailman.getMessage("test", False)
    print (output)
    output = mailman.getMessage("test", False)
    print (output)
    mailman.deleteQueue("test")
    del mailman

def main():
    test()
    exchangeExperiment()


if __name__ == "__main__":
    main()