import requests, json, time

#constants at top
url = "http://10.16.14.137:7474/db/data/"
#Authorization is Base64 encoding of string "username:password"
authorization = "bmVvNGo6cGFzc3dvcmQ="

def getServiceRoot():
    try:
        header = {'Accept': 'application/json', 'Authorization': authorization}
        response = requests.get(url=url, headers = header)
        data = json.loads(response.content.decode())
        return data
    except:
        #basically for connection errors
        print("Something went wrong in getServiceRoot() :(")
        return "Error"

def cypherQueries(queries):
    while (True):
        #need to make sure queries run before continuing to prevent bizarre behaviour
        #without this an agent could be logged as being in many places at once
        try:
            #if the queries succeed we are done so return
            output = cypherQueriesFunction(queries)
            return output
        except:
            #keep trying until queries succeed, otherwise agent can't continue
            continue

def cypherQueriesFunction(queries):
    #First build json file of all queries to be run
    queryList = []
    for i in queries:
        queryList.append({'statement': i})
    payload = {'statements': queryList}
    #Then send request to do all queries
    header = {'Accept': 'application/json', 'Authorization': authorization, 'content-type': 'application/json'}
    response = requests.post(url=url+"transaction/commit", headers=header, data=json.dumps(payload))
    data = json.loads(response.content.decode())
    #if response contains error messages
    if (len(data['errors']) > 0):
        for i in data['errors']:
            print (i)
        raise Exception()
    else:
        return ("Success", data)

#Used to test the functions above and output results to terminal
def test():
    print(getServiceRoot())
    #Add 2 nodes to graph
    list = ["CREATE (n {Name : 'test1'}) RETURN n","CREATE (n {Name : 'test2'}) RETURN n"]
    print(cypherQueries(list))
    #gives enough time to manually check that nodes have been created
    print ("Sleep 5s so you can check these have been added to neo4j")
    time.sleep(5)
    #Delete the nodes added to graph
    list = ["match (n) where n.Name = 'test1' detach delete (n)", "match (n) where n.Name = 'test2' detach delete (n)"]
    print(cypherQueries(list))

def main():
    test()


if __name__ == "__main__":
    main()
