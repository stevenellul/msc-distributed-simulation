import os, time, random, sys
from rabbitmqAPI import messageController
from graphResetGeneration import graphResetGeneration
 
def main():
    #make user enter sudo password now
    os.system("sudo echo > /dev/null")

    print ("\nThis script requires Docker, Neo4J, python3 and RabbitMQ(with durable results queue and exchange) to be preinstalled and preconfigured, also needs to be run from directory it stored in")
    #Capture map size
    print("\nEnter dimensions for map (must be 2 x 2 or greater)")
    #x = input("Enter x axis dimension: ")
    #x = 4
    #y = input("Enter y axis dimension: ")
    #y = 20
    x = sys.argv[3]
    y = sys.argv[4]

    #Capture results filename and number of agents
    #filename = input("\nEnter unique name of file to store results in: ")
    #agentNumber = input("\nEnter number of agents to launch: ")
    filename = sys.argv[1]
    agentNumber = sys.argv[2]

    #Capture container environment variables
    #intelligent = input("\nAre the agents intelligent? Enter True or False: ")
    intelligent = bool(sys.argv[5])
    #neo4jurl = input("\nEnter Neo4J container ip: ")
    #neo4jurl = "http://" + neo4jurl + ":7474/db/data/"
    neo4jurl = "http://10.16.14.137:7474/db/data/"
    #neo4jauth = input("\nEnter Neo4J authorisation (base64 encoding of username:password): ")
    neo4jauth = "bmVvNGo6cGFzc3dvcmQ="
    #rabbitmqurl = input("\nEnter RabbitMQ container ip: ")
    rabbitmqurl = "10.14.216.131"
    #rabbitmqusername = input("\nEnter RabbitMQ username: ")
    rabbitmqusername = "guest"
    #rabbitmqpassword = input("\nEnter RabbitMQ password: ")
    rabbitmqpassword = "guest"

    #create new graph
    graphResetGeneration(int(x), int(y))

    #Pull docker image
    #print ("\nPulling docker agent image:")
    #os.system("sudo docker pull stevedaozy/mscagent > /dev/null")

    #create RabbitMQ exchanges, one for each node
    print ("\nCreate exchange for each node on RabbitMQ")
    mailman = messageController()
    for i in range (1, (int(x)*int(y))+1):
        mailman.createExchange(str(i), 'fanout')
    print ("\nClear old queue for each agent on RabbitMQ")
    for i in range (1, int(agentNumber)+1):
        mailman.createQueue(str(i))
        mailman.clearQueue(str(i))

    #store number of containers already running so can check when all have launched
    initialContainerCount = os.popen("sudo docker info | grep Running | awk '{print $2}'").read()
    
    #make sure results queue is not bound to results exchange so does not start receiving results yet
    mailman.unbindQueueFromExchange('results', 'results')

    #Launch agents
    print ("\nLaunching agents:")
    for i in range (1, int(agentNumber) + 1):
        #generate start and travel nodes randomly
        start = random.randint(1, int(x)*int(y))
        travel = random.randint(1, int(x)*int(y))
        #ensure travel is different to start to prevent errors
        while (start == travel):
            travel = random.randint(1, int(x)*int(y))

        #build command nohup & so runs in background and doesn't print output to terminal
        #append output to file so don't see in terminal and run in background so can launch many
        command = "sudo docker run" + \
                " --name=agent" + str(i) + \
                " --env AGENTID=" + str(i) + \
                " --env STARTNODE=" + str(start) + \
                " --env TRAVELNODE=" + str(travel) + \
                " --env INTELLIGENT=" + str(intelligent) + \
                " --env NEO4JURL=" + neo4jurl + \
                " --env NEO4JAUTH=" + neo4jauth + \
                " --env RABBITMQURL=" + rabbitmqurl + \
                " --env RABBITMQPORT=" + "5672" + \
                " --env RABBITMQVHOST=" + "/" + \
                " --env RABBITMQUSERNAME=" + rabbitmqusername + \
                " --env RABBITMQPASSWORD=" + rabbitmqpassword + \
                " stevedaozy/mscagent > /dev/null 2> /dev/null &"
        #Launch command
        os.system(command)
        if (i%10 == 0):
            print ("Launched", i, "/", agentNumber, "agents")
    print ("Launched", agentNumber, "/", agentNumber, "agents") 
    
    #wait until all agents are running before continuing
    print ("\nWaiting until all agents are running before continuing, please check manually too as if unexpected error this script will run indefinitely")
    runningAgentCount = os.popen("sudo docker info | grep Running | awk '{print $2}'").read()
    while (int(runningAgentCount) != int(agentNumber)+int(initialContainerCount)):
        runningAgentCount = os.popen("sudo docker info | grep Running | awk '{print $2}'").read()
        time.sleep(5)

    #bind results queue to results exchange so can start receiving useful values
    mailman.bindQueueToExchange('results', 'results')
    mailman.bindQueueToExchange('results', 'results')

    #clear results queue because initial readings could be when not all agents were running
    print ("\nClearing useless results")
    mailman.clearQueue("results")
    mailman.clearQueue("results")

    #sleep for 90 seconds to allow collection of data for 60 seconds
    print ("\nScript sleeping for 90 seconds to allow collection of data")
    time.sleep(90)

    #unbind from queue again so whilst containers shut down we do not collect invalid results
    mailman.unbindQueueFromExchange('results', 'results')
    mailman.unbindQueueFromExchange('results', 'results')

    #launch script in new terminal to collect results whilst shutting down containers, do this after experiment so dont affect results
    print ("\nStore results")
    os.system("python3 getResults.py " + str(agentNumber) + " " + str(filename))

    #Remove containers. If following command gives errors add user to docker group with "sudo usermod -a -G docker $USER"
    #print ("\nStopping all agent containers")
    #os.system("sudo docker ps -a | awk '{ print $1,$2 }' | grep mscagent | awk '{print $1 }' | xargs -I {} docker stop {} > /dev/null")
    print ("\nForce removing all agent containers")
    os.system("sudo docker ps -a | awk '{ print $1,$2 }' | grep mscagent | awk '{print $1 }' | xargs -I {} docker rm -f {} > /dev/null")

    print ("\nWaiting until all agents are removed before continuing, please check manually too as if unexpected error this script will run indefinitely")
    runningAgentCount = os.popen("sudo docker info | grep Running | awk '{print $2}'").read()
    while (int(runningAgentCount) != int(initialContainerCount)):
        runningAgentCount = os.popen("sudo docker info | grep Running | awk '{print $2}'").read()
        time.sleep(5)

if __name__ == "__main__":
    main()
