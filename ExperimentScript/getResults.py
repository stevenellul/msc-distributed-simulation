import sys, os, json
from rabbitmqAPI import messageController

def main():
    #connect to rabbitmq
    mailman = messageController()

    #check runtime arguments
    if len(sys.argv) != 3:
        print ("Need to enter 1 runtime parameter:\n1. Number of agents\n2. filename")
        return

    #filename = input("Enter filename to store results in: ")
    #use runtime arguments
    numOfAgents = sys.argv[1]
    filename = sys.argv[2]

    #create array big enough for all results
    results = [(None, None)] * int(numOfAgents)

    print ("\nCollecting results")
    while (True):
        #whilst there are still messages to consume
        try:
            message = json.loads(mailman.getMessage('results', True)[2].decode("utf-8"))
            results[int(message['Agent'])-1] = (int(message['RefreshRate']), int(message['RouteLength']))
        #when no more messages to consume stop
        except:
            break

    #create file to store results, w for write, + for create if doesn't exist
    print ("\nStoring results")
    file = open(filename + ".csv", "w+")
    for i in results:
        file.write("\n" + str(i[0]) + "," + str(i[1]))
    file.close()

    print("\nResults stored in " + filename + ".csv")
    #input("Press any key to exit")

    #when finished disconnect from rabbitmq
    del mailman

    #print ("on new line everytime", end="\r")


if __name__ == "__main__":
    main()