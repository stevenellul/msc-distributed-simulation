from neo4jAPI import cypherQueries
import sys

def graphResetGeneration(xValue, yValue):
    #check runtime parameters
    # if (len(sys.argv) != 3):
    #     print ("Supply 2 arguments for graph size:")
    #     print ("1. How many nodes on x axis")
    #     print ("2. How many nodes on y axis")
    #     exit(2)
    # x = int(sys.argv[1])
    # y = int(sys.argv[2])
    x = int(xValue)
    y = int(yValue)

    #delete existing nodes and relationships
    print ("\nDeleting all nodes and relationships")
    cypherQueries(["MATCH (n) DETACH DELETE n"])
    print ("Node and relationships deleted")

    #list to store all the cypher commands as they're generated
    list = []

    #create nodes
    print ("\nCreating nodes")
    #generate all nodes
    for i in range (1, (x*y)+1):
        #add query to generate particular node to list
        list.append("create (n:Node{Name:'" + str(i) + "'})")
        #send max 100 queries at a time otherwise might fail
        if (len(list) == 100):
            #send list of queries
            cypherQueries(list)
            #clear list and continue with next 100
            list.clear()
            print ("Created ", i, "/", x*y, "nodes")
    #make sure all queries are sent
    if (len(list)!=0):
        cypherQueries(list)
        print ("Created ", i, "/", x*y, "nodes")
        list.clear()
    print ("Nodes created")

    #store count of how many relationships created for user feedback
    count = 0
    #add relationships
    print ("\nAdding relationships")
    for i in range (0, x):
        for j in range (0, y):
            #calculate id of node we are generating relationships for
            id = 1+j+(i*y)
            #calculate id of node before current node
            a = id-1
            #calculate id of node after current node
            b = id+1
            #calculate id of node above current node
            c = id-x
            #calculate id of node below current node
            d = id+x
            #don't create previous node relationship for first node in row of nodes
            if (id % x) != 1:
                list.append("match (n),(x) where n.Name='"+str(id)+"' and x.Name='"+str(a)+"' create (n)-[r:goesto]->(x)")
            #don't create next node relationship for last node in row of nodes
            if (id % x) != 0:
                list.append("match (n),(x) where n.Name='"+str(id)+"' and x.Name='"+str(b)+"' create (n)-[r:goesto]->(x)")
            #don't create above node relationship for nodes in first row
            if not (id <= x):
                list.append("match (n),(x) where n.Name='"+str(id)+"' and x.Name='"+str(c)+"' create (n)-[r:goesto]->(x)")
            #don't create below node relationship for nodes in last row
            if not (id > (x*y)-x):
                list.append("match (n),(x) where n.Name='"+str(id)+"' and x.Name='"+str(d)+"' create (n)-[r:goesto]->(x)")
        if (len(list) > 100):
            cypherQueries(list)
            count += len(list)
            list.clear()
            print ("Created ", count, "/", x*y*4 - (x+x+y+y), "relationships")
    #make sure all queries are sent
    if (len(list)!=0):
        cypherQueries(list)
        count += len(list)
        print ("Created ", count, "/", x*y*4 - (x+x+y+y), "relationships")
        list.clear()
    print ("Relationships created")


if __name__ == "__main__":
    graphResetGeneration()