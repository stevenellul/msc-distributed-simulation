import pika

#constants at top
url = "10.14.216.131"
port = 5672
vHost = '/'
credentials = pika.PlainCredentials('guest', 'guest')

#class only has error handling for things that will affect functionality, any other crashes will require a restart so not handled
class messageController:
    #Creates connection on construction
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(url, port, vHost, credentials))
        self.channel = self.connection.channel()
    #Close connection on destruction
    def __del__(self):
        #No longer close connection because I think this happens regardless
        None
        #self.connection.close()
    def createQueue(self, desiredQueue):
        try:
            self.channel.queue_declare(queue = desiredQueue)
        except:
            self.troubleshoot()
    def clearQueue(self, desiredQueue):
        try:
            self.channel.queue_purge(queue = desiredQueue)
        except:
            self.troubleshoot()
    def createExchange(self, desiredExchange, exchangeType):
        try:
            self.channel.exchange_declare(exchange = desiredExchange, exchange_type = exchangeType)
        except:
            self.troubleshoot()
    def deleteExchange(self, desiredExchange):
        try:
            self.channel.exchange_delete(exchange = desiredExchange)
        except:
            self.troubleshoot()
    def bindQueueToExchange(self, desiredQueue, desiredExchange):
        try:
            self.channel.queue_bind(queue = desiredQueue, exchange = desiredExchange)
        except:
            self.troubleshoot()
    def unbindQueueFromExchange(self, desiredQueue, desiredExchange):
        try:
            self.channel.queue_unbind(queue = desiredQueue, exchange = desiredExchange)
        except:
            self.troubleshoot()
    def deleteQueue(self, desiredQueue):
        try:
            self.channel.queue_delete(queue = desiredQueue)
        except:
            self.troubleshoot()
    def sendMessage(self, desiredQueue, desiredExchange, message):
        try:
            self.channel.basic_publish(exchange = desiredExchange, routing_key = desiredQueue, body = message)
        except:
            self.troubleshoot()
    def getMessage(self, desiredQueue, acknowledge):
        try:
            #no_ack = true flag means auto-acknowledge, no acknowledgement needed, remove it to manually acknowledge messages
            method_frame, header_frame, body = self.channel.basic_get(desiredQueue, no_ack = acknowledge)
            return (method_frame, header_frame, body)
        except:
            #when a getMessage fails the channel is closed, so here we reopen it
            self.channel = self.connection.channel()
            #essentially notifies that there is no queue or message
            return (None, None, None)
    def troubleshoot(self):
        #Connections are being disconnected after amount of time so reconnect 
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(url, port, vHost, credentials))
        self.channel = self.connection.channel()

def exchangeExperiment():
    print ("hello :)")
    mailman = messageController()
    mailman.createExchange("hello", 'fanout')
    mailman.createQueue("1")
    mailman.clearQueue("1")
    mailman.createQueue("2")
    mailman.unbindQueueFromExchange("2","hello")
    mailman.sendMessage('', 'hello', "message")
    mailman.deleteExchange('hello')

def test():
    mailman = messageController()
    mailman.createQueue("test")
    mailman.sendMessage("test", "test", "hey guys")

    #incorrect get message from non existent queue closes channel
    #sendmessage is fine, create queue is fine,  delete queue is fine, acknowledge is fine, reject is fine
    output = mailman.getMessage("tst", False)

    output = mailman.getMessage("test", False)
    #proper way to decode messages
    print (output[2].decode('utf-8'))
    output = mailman.getMessage("test", False)
    print (output)
    output = mailman.getMessage("test", False)
    print (output)
    mailman.deleteQueue("test")
    del mailman

def main():
    exchangeExperiment()


if __name__ == "__main__":
    main()
