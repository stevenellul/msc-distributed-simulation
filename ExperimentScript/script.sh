#! /bin/bash

#python3 script.py filename numberOfAgents xGraphSize yGraphSize intelligence 

#Experiment1

python3 script.py results/experiment1/1/1 1 3 20 False
python3 script.py results/experiment1/1/2 1 3 20 False
python3 script.py results/experiment1/1/3 1 3 20 False
python3 script.py results/experiment1/1/4 1 3 20 False
python3 script.py results/experiment1/1/5 1 3 20 False

cat results/experiment1/1/*.csv > results/experiment1/1/merged.csv

python3 script.py results/experiment1/50/1 50 3 20 False
python3 script.py results/experiment1/50/2 50 3 20 False
python3 script.py results/experiment1/50/3 50 3 20 False
python3 script.py results/experiment1/50/4 50 3 20 False
python3 script.py results/experiment1/50/5 50 3 20 False

cat results/experiment1/50/*.csv > results/experiment1/50/merged.csv

python3 script.py results/experiment1/100/1 100 3 20 False
python3 script.py results/experiment1/100/2 100 3 20 False
python3 script.py results/experiment1/100/3 100 3 20 False
python3 script.py results/experiment1/100/4 100 3 20 False
python3 script.py results/experiment1/100/5 100 3 20 False

cat results/experiment1/100/*.csv > results/experiment1/100/merged.csv

python3 script.py results/experiment1/200/1 200 3 20 False
python3 script.py results/experiment1/200/2 200 3 20 False
python3 script.py results/experiment1/200/3 200 3 20 False
python3 script.py results/experiment1/200/4 200 3 20 False
python3 script.py results/experiment1/200/5 200 3 20 False

cat results/experiment1/200/*.csv > results/experiment1/200/merged.csv

python3 script.py results/experiment1/300/1 300 3 20 False
python3 script.py results/experiment1/300/2 300 3 20 False
python3 script.py results/experiment1/300/3 300 3 20 False
python3 script.py results/experiment1/300/4 300 3 20 False
python3 script.py results/experiment1/300/5 300 3 20 False

cat results/experiment1/300/*.csv > results/experiment1/300/merged.csv


#Experiment 5

python3 script.py results/experiment5/1/1 1 3 20 True
python3 script.py results/experiment5/1/2 1 3 20 True
python3 script.py results/experiment5/1/3 1 3 20 True
python3 script.py results/experiment5/1/4 1 3 20 True
python3 script.py results/experiment5/1/5 1 3 20 True

cat results/experiment5/1/*.csv > results/experiment5/1/merged.csv

python3 script.py results/experiment5/50/1 50 3 20 True
python3 script.py results/experiment5/50/2 50 3 20 True
python3 script.py results/experiment5/50/3 50 3 20 True
python3 script.py results/experiment5/50/4 50 3 20 True
python3 script.py results/experiment5/50/5 50 3 20 True

cat results/experiment5/50/*.csv > results/experiment5/50/merged.csv

python3 script.py results/experiment5/100/1 100 3 20 True
python3 script.py results/experiment5/100/2 100 3 20 True
python3 script.py results/experiment5/100/3 100 3 20 True
python3 script.py results/experiment5/100/4 100 3 20 True
python3 script.py results/experiment5/100/5 100 3 20 True

cat results/experiment5/100/*.csv > results/experiment5/100/merged.csv

python3 script.py results/experiment5/200/1 200 3 20 True
python3 script.py results/experiment5/200/2 200 3 20 True
python3 script.py results/experiment5/200/3 200 3 20 True
python3 script.py results/experiment5/200/4 200 3 20 True
python3 script.py results/experiment5/200/5 200 3 20 True

cat results/experiment5/200/*.csv > results/experiment5/200/merged.csv

python3 script.py results/experiment5/300/1 300 3 20 True
python3 script.py results/experiment5/300/2 300 3 20 True
python3 script.py results/experiment5/300/3 300 3 20 True
python3 script.py results/experiment5/300/4 300 3 20 True
python3 script.py results/experiment5/300/5 300 3 20 True

cat results/experiment5/300/*.csv > results/experiment5/300/merged.csv

#Experiment 4

python3 script.py results/experiment4/1/40/1 1 3 40 False
python3 script.py results/experiment4/1/40/2 1 3 40 False
python3 script.py results/experiment4/1/40/3 1 3 40 False
python3 script.py results/experiment4/1/40/4 1 3 40 False
python3 script.py results/experiment4/1/40/5 1 3 40 False

cat results/experiment4/1/40/*.csv > results/experiment4/1/40/merged.csv

python3 script.py results/experiment4/50/40/1 50 3 40 False
python3 script.py results/experiment4/50/40/2 50 3 40 False
python3 script.py results/experiment4/50/40/3 50 3 40 False
python3 script.py results/experiment4/50/40/4 50 3 40 False
python3 script.py results/experiment4/50/40/5 50 3 40 False

cat results/experiment4/50/40/*.csv > results/experiment4/50/40/merged.csv

python3 script.py results/experiment4/100/40/1 100 3 40 False
python3 script.py results/experiment4/100/40/2 100 3 40 False
python3 script.py results/experiment4/100/40/3 100 3 40 False
python3 script.py results/experiment4/100/40/4 100 3 40 False
python3 script.py results/experiment4/100/40/5 100 3 40 False

cat results/experiment4/100/40/*.csv > results/experiment4/100/40/merged.csv

python3 script.py results/experiment4/200/40/1 200 3 40 False
python3 script.py results/experiment4/200/40/2 200 3 40 False
python3 script.py results/experiment4/200/40/3 200 3 40 False
python3 script.py results/experiment4/200/40/4 200 3 40 False
python3 script.py results/experiment4/200/40/5 200 3 40 False

cat results/experiment4/200/40/*.csv > results/experiment4/200/40/merged.csv

python3 script.py results/experiment4/300/40/1 300 3 40 False
python3 script.py results/experiment4/300/40/2 300 3 40 False
python3 script.py results/experiment4/300/40/3 300 3 40 False
python3 script.py results/experiment4/300/40/4 300 3 40 False
python3 script.py results/experiment4/300/40/5 300 3 40 False

cat results/experiment4/300/40/*.csv > results/experiment4/300/40/merged.csv

python3 script.py results/experiment4/1/60/1 1 3 60 False
python3 script.py results/experiment4/1/60/2 1 3 60 False
python3 script.py results/experiment4/1/60/3 1 3 60 False
python3 script.py results/experiment4/1/60/4 1 3 60 False
python3 script.py results/experiment4/1/60/5 1 3 60 False

cat results/experiment4/1/60/*.csv > results/experiment4/1/60/merged.csv

python3 script.py results/experiment4/50/60/1 50 3 60 False
python3 script.py results/experiment4/50/60/2 50 3 60 False
python3 script.py results/experiment4/50/60/3 50 3 60 False
python3 script.py results/experiment4/50/60/4 50 3 60 False
python3 script.py results/experiment4/50/60/5 50 3 60 False

cat results/experiment4/50/60/*.csv > results/experiment4/50/60/merged.csv

python3 script.py results/experiment4/100/60/1 100 3 60 False
python3 script.py results/experiment4/100/60/2 100 3 60 False
python3 script.py results/experiment4/100/60/3 100 3 60 False
python3 script.py results/experiment4/100/60/4 100 3 60 False
python3 script.py results/experiment4/100/60/5 100 3 60 False

cat results/experiment4/100/60/*.csv > results/experiment4/100/60/merged.csv

python3 script.py results/experiment4/200/60/1 200 3 60 False
python3 script.py results/experiment4/200/60/2 200 3 60 False
python3 script.py results/experiment4/200/60/3 200 3 60 False
python3 script.py results/experiment4/200/60/4 200 3 60 False
python3 script.py results/experiment4/200/60/5 200 3 60 False

cat results/experiment4/200/60/*.csv > results/experiment4/200/60/merged.csv

python3 script.py results/experiment4/300/60/1 300 3 60 False
python3 script.py results/experiment4/300/60/2 300 3 60 False
python3 script.py results/experiment4/300/60/3 300 3 60 False
python3 script.py results/experiment4/300/60/4 300 3 60 False
python3 script.py results/experiment4/300/60/5 300 3 60 False

cat results/experiment4/300/60/*.csv > results/experiment4/300/60/merged.csv

python3 script.py results/experiment4/1/80/1 1 3 80 False
python3 script.py results/experiment4/1/80/2 1 3 80 False
python3 script.py results/experiment4/1/80/3 1 3 80 False
python3 script.py results/experiment4/1/80/4 1 3 80 False
python3 script.py results/experiment4/1/80/5 1 3 80 False

cat results/experiment4/1/80/*.csv > results/experiment4/1/80/merged.csv

python3 script.py results/experiment4/50/80/1 50 3 80 False
python3 script.py results/experiment4/50/80/2 50 3 80 False
python3 script.py results/experiment4/50/80/3 50 3 80 False
python3 script.py results/experiment4/50/80/4 50 3 80 False
python3 script.py results/experiment4/50/80/5 50 3 80 False

cat results/experiment4/50/80/*.csv > results/experiment4/50/80/merged.csv

python3 script.py results/experiment4/100/80/1 100 3 80 False
python3 script.py results/experiment4/100/80/2 100 3 80 False
python3 script.py results/experiment4/100/80/3 100 3 80 False
python3 script.py results/experiment4/100/80/4 100 3 80 False
python3 script.py results/experiment4/100/80/5 100 3 80 False

cat results/experiment4/100/80/*.csv > results/experiment4/100/80/merged.csv

python3 script.py results/experiment4/200/80/1 200 3 80 False
python3 script.py results/experiment4/200/80/2 200 3 80 False
python3 script.py results/experiment4/200/80/3 200 3 80 False
python3 script.py results/experiment4/200/80/4 200 3 80 False
python3 script.py results/experiment4/200/80/5 200 3 80 False

cat results/experiment4/200/80/*.csv > results/experiment4/200/80/merged.csv

python3 script.py results/experiment4/300/80/1 300 3 80 False
python3 script.py results/experiment4/300/80/2 300 3 80 False
python3 script.py results/experiment4/300/80/3 300 3 80 False
python3 script.py results/experiment4/300/80/4 300 3 80 False
python3 script.py results/experiment4/300/80/5 300 3 80 False

cat results/experiment4/300/80/*.csv > results/experiment4/300/80/merged.csv

touch finished.txt