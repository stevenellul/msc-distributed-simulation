# MSc Project Repository #

This README contains the steps that are necessary to get this project up and running

### What is this repository for? ###

This is the source code for my MSc project titled 'Agent-based Modelling for Distributed Crowd Simulation'

The rough idea is that this project simulates agents traversing a map (built in Neo4J), with each agent checking where nearby agents are going to avoid congestion (messaging via RabbitMQ), with each agent running in its own container; purpose is to test scalability of the design.

### How do I get set up? ###

1. Configure a Docker server and install Python3 with requests and pika libraries, project must be cloned to this server
2. Configure a RabbitMQ server
3. Configure a Neo4J server
4. (Optional) If one desires, they can also build the agent container instead of using the one from the DockerHub repository that the script automatically acquires:
	1. On the Docker server, navigate to the Docker directory within this project
	2. Then execute the following command taking care to include the dot at the end which is path to DockerFile: 
		- docker build -t CONTAINER-NAME .
5. The simulation configuration must be edited from the Docker server from the ExperimentScript directory:
	1. Lines 4 and 7 in rabbitmqAPI.py must be amended to reflect the RabbitMQ server configuration
	2. Lines 4 and 6 in neo4jAPI.py must be amended to reflect the Neo4J server configuration
	3. Lines 30 and 32 in script.py must be amended to reflect the Neo4J server configuration
	4. Lines 34, 36 and 38 in script.py must be amended to reflect the RabbitMQ server configuration
	
### How do I run the project? ###

To run the simulation, edit and launch the script.sh file in ExperimentScript directory using the following format:

- python3 script.py param1 param2 param3 param4 param5
	* param1 is filename of results log (String)
	* param2 is number of agents to launch (Integer)
	* param3 is x dimension of map (Integer)
	* param4 is y dimension of map (Integer)
	* param5 is intelligence of agent (True or False)